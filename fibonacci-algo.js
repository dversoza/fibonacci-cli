class FibonacciSequence {
    constructor(num) {
        this.result = this.toInt(num);
    }

    // calculates the Fibonacci's sequence using recursion (too slow)
    // toInt(num) {
    //     if (num === 0) {
    //         return 0;
    //     }
    //     if (num === 1) {
    //         return 1;
    //     }
    //     return this.toInt(num - 1) + this.toInt(num - 2);
    // }

    // calculates Fibonacci's sequence using while loop (faster)
    toInt(num) {
        let fibo = 0;
        let fibo1 = 1;
        let fibo2 = 0;
        while (num > 0) {
            fibo2 = fibo1;
            fibo1 = fibo;
            fibo = fibo1 + fibo2;
            num--;
        }
        return fibo;
    }

}

module.exports = FibonacciSequence;
