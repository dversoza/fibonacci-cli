import FibonacciSequence from "./fibonacci-algo";

describe('Fibonacci Sequence', () => {
  test('should return 0 for 0', () => {
    const fibo = new FibonacciSequence(0);
    expect(fibo.result).toBe(0);
  });

  test('should return 1 for 1', () => {
    const fibo = new FibonacciSequence(1);
    expect(fibo.result).toBe(1);
  });

  test('should return 1 for 2', () => {
    const fibo = new FibonacciSequence(2);
    expect(fibo.result).toBe(1);
  });

  test('should return 2 for 3', () => {
    const fibo = new FibonacciSequence(3);
    expect(fibo.result).toBe(2);
  });

  test('should return 3 for 4', () => {
    const fibo = new FibonacciSequence(4);
    expect(fibo.result).toBe(3);
  });

  test('should return 5 for 5', () => {
    const fibo = new FibonacciSequence(5);
    expect(fibo.result).toBe(5);
  });

  test('should return 8 for 6', () => {
    const fibo = new FibonacciSequence(6);
    expect(fibo.result).toBe(8);
  });

  test('should return 13 for 7', () => {
    const fibo = new FibonacciSequence(7);
    expect(fibo.result).toBe(13);
  });
});
