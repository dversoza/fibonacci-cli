#!/usr/bin/env node

const yargs = require('yargs');
const FibonacciSequence = require('./fibonacci-algo.js');

module.exports = () => {
    const options = yargs
        .usage("Usage: -n <number>")
        .option("n", {
            alias: "number",
            describe: "The number of the fibonacci sequence to calculate",
            type: "string",
            demandOption: true
        })
        .argv;

    const fibo = new FibonacciSequence(options.n);

    console.log(fibo.result);
}
